# docker-sec-example

This repository contains a Dockerfile which extends the official nginx container image to create a microservice. The microservice presents a website advertising some example services.

To build the container locally:

`docker build --tag local/sec-example .`

To run the container in the foreground:

`docker run -p 80:80 -p 443:443 --interactive --tty local/sec-example`

Once the container is running you should be able to navigate to <http://localhost> or <https://localhost> to view it.

Note about ports:

* If you're attempting to use privileged ports you may find that you get an error when starting the container, if this is the case I suggest you use the following instead:
  * `docker run -p 8080:80 -p 8443:443 --interactive --tty local/sec-example`
  * Navigate to <http://localhost:8080> or <https://localhost:8443>

The purpose of this exercise is to ensure a secure workload is being deployed for this microservice. You should undertake the following tasks:

* Extend the GitLab CI pipeline to run appropriate scans to ensure sufficient security
  * It's up to you what tooling you use; please research, select appropriately and document your reasons
* Write down at least 2 general advisories for improving the security of the container (i.e. not the output of the security tooling)

**Important notes:**

* Please don't spend longer than 2 hours completing this task.
* Don't hide your test commits or pipelines - it's very useful to see the evolution of your creation.
* You don't need to fix the issues that are uncovered.

*Hint: Check out what tooling is provided by GitLab first*

## CI Pipeline

* Summary notes can be found in.gitlab-ci.yml file.

## Container Security General Advisories

### Keep the Docker environment up to date and minimal

* The Docker image nginx:1.18.0-perl is an outdated version. The latest version being nginx:1.22.0-perl

* The OS running is Debian (Buster) 10.9. There are 76 identified Linux system packages with vulnerbilities that could potentially be fixed by updating the version of Debian to a later Buster release (i.e. 10.13) or upgrading to (Bullseye) version 11 etc.

* Although most of the packages with vulnerabilities were Linux system applications, if any are unnecessary/ not critical to Debian, then they should be uninstalled. Some of these might not be critical for the deployment of the microservice.

### Run the container without Root privileges

* Avoid running docker containers with root privileges, to prevent privilege escalation attacks.

* Create a user with none root permissions (or add to sudo or specific groups depending on the specific privileges required). For example, add the following to the Dockerfile:

` Run groupadd -r <group_name> && useradd -r -g <group_name> <username> `

and for sudo:

` Run groupadd -r <username> && useradd -r -g sudo <username> `

### Keep Host and Docker up to date

* Ensure both are patched to mitigate container escapes.

* The kernel of the host OS and the container are shared so a kernel exploit can affect both. This can lead to both the container and the host OS being compromised.
