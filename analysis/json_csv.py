import json
import pandas as pd

with open("./gl-container-scanning-report.json", "r") as file_data:
    data_dict = json.load(file_data)

df = pd.DataFrame()

for row in data_dict["vulnerabilities"]:
    df_temp = pd.DataFrame({ "cve": [row["cve"]],
                            "severity": [row["severity"]],
                            "name": [row["location"]["dependency"]["package"]["name"]],
                            "version": [row["location"]["dependency"]["version"]],
                            "os": [row["location"]["operating_system"]]})
    df= df.append(df_temp)

df.to_csv("./outputfile.csv")

